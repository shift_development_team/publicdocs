---
tags: [Booking]
---

# Booking Statuses

Bookings will go through several status transitions until it ends. If your application is using [Webhooks](url) you will receive a notification for each status change.

| Status name         | Description                                                                                                                                       |
| ------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------- |
| Pending             | This means the booking has not yet been allocated to a driver, or perhaps a driver has unassigned and now the system is looking for a new driver.|
| Accepted            | The booking has been accepted by a Driver on Shifts platform.|
| On Route to Pickup  | The driver is on there way to the pickup location|
| Arrived at Pickup   | The driver has arrived at the pickup location|
| Collected at Pickup | Everything has been loaded at the pickup location and the driver is now leaving |
| On Route to Dropoff | The driver is on there way to the dropoff location|
| Arrived at Dropoff  | The driver has arrived at the dropoff location|
| Completed           | The service has now been completed|
| Cancelled           | The entire service has been cancelled|
| Scheduled           | The service has not been cancelled, but is unable to take place, usually due to missing information. This status is used as a to hold the booking for a period of time.|
